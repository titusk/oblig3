package edu.ntnu.idatt2001.Oblig3.cardgame;

import javafx.application.Application;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.text.Text;

import java.util.ArrayList;

/**
 * Class Client
 * @version 1.0
 * @author Titus Kristiansen
 */
public class Client extends Application {

    /**
     * Main method
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Variable creation
     */
    private ArrayList<PlayingCard> cards;
    private DeckOfCards deck;
    private Text showCardsText;
    private Text isFlushText;
    private Text isSumOfCardsText;
    private Text isHeartCardsText;
    private Text isQueenOfSpadesText;

    /**
     * start method
     * JavaFX starter method
     */
    @Override
    public void start(Stage primaryStage) {

        /**
         * Variable initialisation
         */
        deck = new DeckOfCards();
        cards = new ArrayList<>();

        Button dealHandButton = new Button("Deal hand");
        Button checkHandButton = new Button("Check hand");

        Text sumOfCardsText = new Text("Sum of cards:");
        Text heartCardsText = new Text("Hearts:");
        Text queenOfSpadesText = new Text("Queen of spades:");
        Text flushText = new Text("Flush:");

        showCardsText = new Text("");
        isSumOfCardsText = new Text("");
        isHeartCardsText = new Text("");
        isQueenOfSpadesText = new Text("");
        isFlushText = new Text("");

        /**
         * Variable layout and styling
         */
        showCardsText.setLayoutX(50);
        isSumOfCardsText.setLayoutX(360);
        isHeartCardsText.setLayoutX(360);
        isQueenOfSpadesText.setLayoutX(360);
        isFlushText.setLayoutX(360);
        sumOfCardsText.setLayoutX(250);
        heartCardsText.setLayoutX(250);
        queenOfSpadesText.setLayoutX(250);
        flushText.setLayoutX(250);
        dealHandButton.setLayoutX(50);
        checkHandButton.setLayoutX(50);

        showCardsText.setLayoutY(100);
        isSumOfCardsText.setLayoutY(250);
        isHeartCardsText.setLayoutY(280);
        isQueenOfSpadesText.setLayoutY(310);
        isFlushText.setLayoutY(340);
        sumOfCardsText.setLayoutY(250);
        heartCardsText.setLayoutY(280);
        queenOfSpadesText.setLayoutY(310);
        flushText.setLayoutY(340);
        dealHandButton.setLayoutY(200);
        checkHandButton.setLayoutY(300);

        dealHandButton.setPrefWidth(100);
        checkHandButton.setPrefWidth(100);

        dealHandButton.setPrefHeight(40);
        checkHandButton.setPrefHeight(40);

        showCardsText.setStyle("-fx-font: 30 arial");

        /**
         * Button actions
         */
        dealHandButton.setOnAction(e -> setShowCards());
        checkHandButton.setOnAction(e -> checkCards());

        /**
         * AnchorPane creation and data inserting
         */
        AnchorPane gameAnchorPane = new AnchorPane();

        gameAnchorPane.getChildren().addAll(showCardsText, dealHandButton, checkHandButton, flushText, isFlushText,
                sumOfCardsText, heartCardsText, queenOfSpadesText, isHeartCardsText, isQueenOfSpadesText,
                isSumOfCardsText);

        /**
         * Scene creation
         */
        Scene gameScene = new Scene(gameAnchorPane, 500, 500);

        /**
         * Stage initialisation
         */
        try {
            primaryStage.setTitle("Card Game");
            primaryStage.setScene(gameScene);
            primaryStage.show();

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * setShowCards method
     * sets the Text showCardsText to what the dealt cards are
     */
    private void setShowCards(){
        cards = deck.dealHand(5);
        StringBuilder showCardsString = new StringBuilder();
        for (PlayingCard card : cards) {
            showCardsString.append(card.getAsString()).append(" ");
        }
        showCardsText.setText(showCardsString.toString());
    }

    /**
     * checkSumOfCards
     * sets the Text isSumOfCards to what the sum of all faces of dealt cards is
     */
    private void checkSumOfCards(){
        ArrayList<Integer> cardSumArray = new ArrayList();
        for (PlayingCard card : cards){
            cardSumArray.add(card.getFace());}
        isSumOfCardsText.setText(String.valueOf(cardSumArray.stream().reduce(0, (el1, el2) -> el1 + el2)));
    }

    /**
     * checkHeartCards method
     * sets the Text isHeartCardsText to heart cards from the dealt hand, otherwise diplays "No hearts"
     */
    private void checkHeartCards(){
        StringBuilder heartCardsString = new StringBuilder();
        for (PlayingCard card : cards) {
            if (card.getAsString().contains("♥")) {
                heartCardsString.append(card.getAsString()).append(" ");
            }
        }
        if (heartCardsString.toString().equals("")){
            isHeartCardsText.setText("No hearts");
        }
        else isHeartCardsText.setText(heartCardsString.toString());
    }

    /**
     * checkQueenOfSpades method
     * sets the Text isQueenOfSpadesText to one String if there is a queen of spades is in the dealt hand,
     * otherwise displays a different String
     */
    private void checkQueenOfSpades(){
        isQueenOfSpadesText.setText("THE QUEEN IS DEAD");
        for (PlayingCard card : cards) {
            if (card.getAsString().equals("♦12")) {
                isQueenOfSpadesText.setText("THE QUEEN IS ALIVE!");
            }
        }
    }

    /**
     * checkFlush method
     * sets the Text isFlushText to a String if the dealt hand is a flush, otherwise displays "No flush"
     */
    private void checkFlush(){

        boolean loopStopper = Boolean.TRUE;
        Character suitChecker = cards.get(0).getSuit();
        while (loopStopper) {
            for (int i = 1; i < cards.size(); i++) {
                if (!suitChecker.equals(cards.get(i).getSuit())) {
                    loopStopper = false;
                    break;
                }
            }
        }
        if (loopStopper){
            isFlushText.setText("FLUSH!");
        }
        else isFlushText.setText("No flush");
    }

    /**
     * checkCards method
     * Runs all of the Text settings and thus displays data about the dealt hand of cards
     */
    private void checkCards(){
        checkSumOfCards();
        checkHeartCards();
        checkQueenOfSpades();
        checkFlush();
    }
}
package edu.ntnu.idatt2001.Oblig3.cardgame;

/**
 * Class PlayingCard
 * @version 1.0
 * @author Titus Kristiansen
 */
public class PlayingCard{

    /**
     * Variable creation
     */
    private final char suit;
    private final int face;

    /**
     * Constructor
     * @param suit
     * @param face
     */
    public PlayingCard(char suit, int face) {
        this.suit = suit;
        this.face = face;
    }

    /**
     * getAsString getter method
     * @return returns the PlayingCard data as a String
     */
    public String getAsString() {
        return String.format("%s%s", suit, face);
    }

    /**
     * getSuit getter method
     * @return returns the PlayingCard suit as a Character
     */
    public char getSuit() {
        return suit;
    }

    /**
     * getFace getter method
     * @return returns the PlayingCard face as an Integer
     */
    public int getFace() {
        return face;
    }
}
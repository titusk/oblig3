package edu.ntnu.idatt2001.Oblig3.cardgame;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Class DeckOfCards
 * @version 1.0
 * @author Titus Kristiansen
 */
public class DeckOfCards {

    /**
     * Variable creation
     */
    private final char[] suit;
    private final int[] face;

    private ArrayList<PlayingCard> deckOfCards;

    /**
     * Constructor, variable initialisation
     * Creates the deck of cards
     */
    public DeckOfCards(){

        deckOfCards = new ArrayList();
        suit = new char[] {'♠', '♥', '♦', '♣'};
        face = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

        int s=0;
        int f=0;
        for (int i=1; i<53; i++){

            deckOfCards.add(new PlayingCard(suit[s], face[f]));

            if (i%13 == 0){
                s++;
                f=0;
            }
            else f++;
         }
    }

    /**
     * dealHand method
     * deals a hand of cards
     * @param amountOfCards how many cards should be dealt
     * @return the selected amount of random unique cards from the deck
     */
    public ArrayList<PlayingCard> dealHand(int amountOfCards) {
        ArrayList<PlayingCard> shuffledDeck = deckOfCards;
        Collections.shuffle(shuffledDeck);
        ArrayList<PlayingCard> dealtHand = new ArrayList<>();
        for (int i = 0; i < amountOfCards; i++) {
            dealtHand.add(shuffledDeck.get(i));
        }
        return dealtHand;
    }
}